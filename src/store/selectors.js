export const selectIsModalOpen = (state) => state.isModalOpen;
export const selectGoodsCollection = (state) => state.goodsCollection;
export const selectCart = (state) => state.cart;
export const selectFavorite = (state) => state.favorite;
export const selectTryToCart = (state) => state.tryToCart;
export const selectTryToRemoveCart = (state) => state.tryToRemoveCart;
export const selectModalText = (state) => state.modalText;
export const selectModalTitle = (state) => state.modalTitle;
export const selectNeedRemoveItem = (state) => state.needRemoveItem;
export const selectState = (state) => state;

