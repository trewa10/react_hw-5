import {createReducer} from "@reduxjs/toolkit";
import * as actions from './actions'

const initialState = {
    isModalOpen: false,
    goodsCollection: [],
    cart: [],
    favorite: [],
    tryToCart: {},
    tryToRemoveCart: {},
    modalText: '',
    modalTitle: '',
    needRemoveItem: false,
}

export default createReducer(initialState, {
    [actions.actionIsModalOpen]: (state, {payload}) => {
        state.isModalOpen = payload;
    },
    [actions.actionGoodsCollection]: (state, {payload}) => {
        state.goodsCollection = payload;
    },
    [actions.actionCart]: (state, {payload}) => {
        state.cart = payload;
    },
    [actions.actionFavorite]: (state, {payload}) => {
        state.favorite = payload;
    },
    [actions.actionTryToCart]: (state, {payload}) => {
        state.tryToCart = payload;
    },
    [actions.actionTryToRemoveCart]: (state, {payload}) => {
        state.tryToRemoveCart = payload;
    },
    [actions.actionModalText]: (state, {payload}) => {
        state.modalText = payload;
    },
    [actions.actionModalTitle]: (state, {payload}) => {
        state.modalTitle = payload;
    },
    [actions.actionNeedRemoveItem]: (state, {payload}) => {
        state.needRemoveItem = payload;
    },
})