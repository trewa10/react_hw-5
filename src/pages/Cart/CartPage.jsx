import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { selectCart } from '../../store/selectors';
import { actionCart } from '../../store/actions';
import PageWrapper from "../../components/PageWrapper/PageWrapper";
import ItemsList from "../../components/ItemsList";
import BackBtn from '../../components/BackBtn';
import CartForm from '../../components/Form/CartForm/CartForm';



const CartPage = (props) => {
    const dispatch = useDispatch();
    const cart = useSelector(selectCart);
    
    const purchaseSubmit = (value) => {
        value.phone = value.phone.match(/\d+/g).join('');   // видалення маски номера
        if(cart.length !== 0) {       // просто додаткова перевірка на випадок якщо не приховувати форму
            let order = {
                user: value,
                cart: cart
            }
            console.log(order);
            localStorage.removeItem('cart');
            dispatch(actionCart([]));
        } else console.log('Your cart is empty');
    
    }
    const {textBtn, widthBtn, goBack} = props;
    const totalPrice = cart.reduce((acc, item) => acc + +item.price, 0)
    return (
        <PageWrapper>
            <h2><BackBtn goBack={goBack} />Your cart</h2>
            {cart.length > 0 && <CartForm totalPrice={totalPrice} amount={cart.length} onSubmit={purchaseSubmit}/>}
            {cart.length === 0 && <h2>Your cart is empty. Please add some goods</h2>}
            <ItemsList itemsList={cart} isRemove={true} textBtn={textBtn} widthBtn={widthBtn}/>
        </PageWrapper>
    )
}

CartPage.propTypes = {
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string,
    goBack: PropTypes.func
}

export default CartPage;