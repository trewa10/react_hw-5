import PropTypes from 'prop-types';
import PageWrapper from '../../components/PageWrapper/PageWrapper';
import ItemsList from '../../components/ItemsList';
import BackBtn from '../../components/BackBtn';
import { selectFavorite } from '../../store/selectors';
import { useSelector } from 'react-redux';

const FavoritePage = ({textBtn, widthBtn, goBack}) => {
    const favorite = useSelector(selectFavorite);
    return (
        <PageWrapper>
            <h2><BackBtn goBack={goBack} />Your favorite</h2>
            <ItemsList itemsList={favorite} textBtn={textBtn} widthBtn={widthBtn}/>
        </PageWrapper>
    )
}

FavoritePage.propTypes = {
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string,
    goBack: PropTypes.func
}

export default FavoritePage;