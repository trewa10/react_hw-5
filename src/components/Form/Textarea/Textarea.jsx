import { Field, ErrorMessage } from 'formik';
import cx from 'classnames';
import PropTypes from "prop-types";

const Textarea = ({ type, placeholder, label, name, className, error, rows, ...restProps}) => {

    return (
        <label className={cx("form-item", className,{'error-message': error})}>
            <p className="form-label">{label}</p>
            <textarea className="form-control" name={name} rows={rows} placeholder={placeholder} {...restProps} ></textarea>
            {/* <Field as="textarea"></Field> */}
            <ErrorMessage className='error-message' name={name} component='p'/>
        </label>
    )
}

Textarea.propTypes = {
    placeholder: PropTypes.string,
    rows: PropTypes.number,
    label: PropTypes.string,
    name: PropTypes.string,
    className: PropTypes.string,
    error: PropTypes.object,
}

export default Textarea;