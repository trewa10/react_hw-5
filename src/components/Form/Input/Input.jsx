import { Field, ErrorMessage } from 'formik';
import styled, {css} from 'styled-components';
import PropTypes from "prop-types";
import { PatternFormat } from 'react-number-format';


const StyledField = styled(Field)`
    width: 100%;
    height: 35px;
    border-radius: 8px;
    box-sizing: border-box;
    font-size: 18px;

    ${(props) => {
        if(props.error) {
            return css`
                border-color: #dc3545;

                &:hover, &:focus{
                    border-color: #dc3545;
                    box-shadow: 0 0 0 0.25rem rgb(220 53 69 / 25%);
                    }
                `;
            }
        }
    }
`
const FormItem = styled.label`
    display: block;
    height: 100px;
    width: ${props => props.width ? props.width : '250px'};
`
const StyledErrorMessage = styled.p`
    margin: 4px 0 0;
    text-align: left;
    color: #dc3545;
    
`
const FormLabel = styled.p`
    text-align: left;
    margin: 4px 0;
`
const Phone = styled(PatternFormat)`
    width: 100%;
    height: 35px;
    border-radius: 8px;
    box-sizing: border-box;
    font-size: 18px;

    ${(props) => {
        if(props.error) {
            return css`
                border-color: #dc3545;

                &:hover, &:focus{
                    border-color: #dc3545;
                    box-shadow: 0 0 0 0.25rem rgb(220 53 69 / 25%);
                    }
                `;
            }
        }
    }
`

const NumberField = ({ field }) => {
    return <Phone {...field} format="+38 (0##) ###-##-##" allowEmptyFormatting mask="_" />;
  }

const Input = ({type, placeholder, label, name, error, errors, width, phone, ...restProps}) => {

	return (
        <FormItem width={width}>
            <FormLabel>{label}</FormLabel>
            {!phone &&<StyledField type={type} error={error} name={name} placeholder={placeholder} {...restProps} />}
            {phone && <Field type={type} error={error} name={name} {...restProps} component={NumberField}/>}
            <ErrorMessage name={name} component={StyledErrorMessage}/>
        </FormItem>
	)
}

Input.defaultProps = {
    type:'text'
}

Input.propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string,
    error: PropTypes.object,
    width: PropTypes.string,
    phone: PropTypes.bool
}

export default Input