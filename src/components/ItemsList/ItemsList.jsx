import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Card from '../Card/Card';

const ItemWrapper = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    gap: 30px;
    padding-bottom: 45px; 
`

const ItemsList =({itemsList, isRemove, textBtn, widthBtn}) => {
    const cardsRender = itemsList.map(({name, price, imgUrl, article, color, isInFavorite}, index) => {
        return (
            (<Card key={(article + '' + index)} isInFavorite={isInFavorite} name={name} price={price} imgUrl={imgUrl} 
            article={article} color={color} isRemove={isRemove} textBtn={textBtn} widthBtn={widthBtn}/>)
        )
    })
    return (
        <>
            <ItemWrapper>
                {cardsRender}
            </ItemWrapper>
        </>
    )
}

ItemsList.propTypes = {
    itemsList: PropTypes.array.isRequired,
    isRemove: PropTypes.bool,
    addToFavorite: PropTypes.func,
    widthBtn: PropTypes.string
}

export default ItemsList