import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledButton = styled.button`
color: white;
background: ${props => props.backgroundColor ? props.backgroundColor : 'none'};
width: ${props => props.width ? props.width : '100px'};
height: ${props => props.height ? props.height : '40px'};
border: none;
border-radius: 8px;
display: flex;
justify-content: center;
align-items: center;
font-weight: 700;
cursor: pointer;
padding: 0;
`

const Button = ({children, onClick, backgroundColor, width, height, type, ...restProps}) => {
    return <StyledButton type={type ? type : "button"} {...restProps} height={height} backgroundColor={backgroundColor} onClick={onClick} width={width}>{children}</StyledButton>
        
}

Button.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    restProps: PropTypes.array
}

export default Button;