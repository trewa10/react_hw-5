import React, {useEffect} from 'react';
import {createGlobalStyle} from 'styled-components';
import {Route, Routes, useNavigate} from 'react-router-dom';
import Button from './components/Button';
import Modal from './components/Modal';
import Header from './components/Header';
import Footer from './components/Footer';
import MainPage from './pages/MainPage/MainPage';
import CartPage from './pages/Cart/CartPage';
import FavoritePage from './pages/Favorite/FavoritePage';
import { useDispatch, useSelector } from 'react-redux';
import { selectCart, selectIsModalOpen, selectModalText, selectModalTitle, selectNeedRemoveItem, selectTryToCart, selectTryToRemoveCart } from './store/selectors';
import { actionCart, actionFavorite, actionFetchGoodsCollection, actionIsModalOpen } from './store/actions';

const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  #root {
    display: flex;
    flex-direction: column;
    flex: 1 1 auto;
    height: 100%;
  }
`

const App = () => {
  const dispatch = useDispatch();

  const isModalOpen = useSelector(selectIsModalOpen);
  const modalText = useSelector(selectModalText);
  const modalTitle = useSelector(selectModalTitle);
  const cart = useSelector(selectCart);
  const needRemoveItem = useSelector(selectNeedRemoveItem);
  const tryToCart = useSelector(selectTryToCart);
  const tryToRemoveCart = useSelector(selectTryToRemoveCart);

  useEffect(() => {
    dispatch(actionFetchGoodsCollection());
    getCart();
    getFavorite();
  }, [])

  useEffect(() => {
    if (isModalOpen) modalShow();
  }, [isModalOpen]);

  const modalShow = () => document.querySelector('dialog').showModal();

  const closeModal = () => {
    dispatch(actionIsModalOpen(false));
  }
  
  const confirmAddItem = () => {
    let newCart = [...cart];
    let item = {...tryToCart};
    newCart.push(item);
    dispatch(actionCart(newCart));
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }

  const confirmRemoveItem = () => {
    let newCart = [...cart];
    let item = {...tryToRemoveCart};
    let index = newCart.findIndex((card) => card.article === item.article);
    newCart.splice(index, 1);
    dispatch(actionCart(newCart));
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }

  const getCart = () => {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if(cart && cart.length > 0) {
      dispatch(actionCart(cart))
    }
  }

  const getFavorite = () => {
    let favorite = JSON.parse(localStorage.getItem('favorite'));
    if(favorite) {
      dispatch(actionFavorite(favorite))
    }
  }

  const navigate = useNavigate();
  const goBack = () => navigate(-1);

  return (
    <>
      <GlobalStyle/>
      <Header/>
      <Routes>
        <Route index element={<MainPage textBtn='Add to cart' />} />
        <Route path='/cart' element={<CartPage textBtn='Remove from cart' widthBtn='135px' goBack={goBack}/>} />
        <Route path='/favorite' element={<FavoritePage textBtn='Add to cart' goBack={goBack}/>} />
      </Routes>
      
      {isModalOpen && (<Modal closeButton={true} actions={
        <>
          <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={needRemoveItem ? confirmRemoveItem : confirmAddItem}>Yes</Button>
          <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={closeModal}>Cancel</Button>
        </>
      } closeClick={closeModal} backgroundColor='#aeb5b8' header={modalTitle} text={modalText}>
        
      </Modal>)}
      
      <Footer/>
    </>
  )
}


export default App;
